package ito.poo.app;
import java.time.LocalDate;
import ito.poo.clases.Lote;
import ito.poo.clases.Prenda;
@SuppressWarnings("unused")
public class myApp {
	
	static void run() {
		Prenda p1 = new Prenda(125, "algodon", 40.50F, "Masculino", "invierno");
		System.out.println(p1);
		p1.addLote(1, 5300, LocalDate.now());
		p1.addLote(2, 4300, LocalDate.of(2021, 7, 15));
		System.out.println(p1);
		System.out.println(p1.getLote(2));
		System.out.println(p1.getLote(1).costoProduccion(p1));
		System.out.println(p1.getLote(2).costoProduccion(p1));
		System.out.println(p1.getLote(1).montoProduccionxlote(p1));
		System.out.println(p1.getLote(1).montoRecuperacionxpieza(p1));
		}

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		run();
	}

}
